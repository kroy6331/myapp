import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-poll',
  templateUrl: './poll.component.html',
  styleUrls: ['./poll.component.css']
})
export class PollComponent implements OnInit {
  yesIncreamentVal:number = 0;
  noIncreamentVal: number = 0;
  yesIncreament()
  {
   this.yesIncreamentVal++;
    }

  constructor() {
 }
 noIncreament(){
   this.noIncreamentVal++
 }

  ngOnInit() {
  }

}
