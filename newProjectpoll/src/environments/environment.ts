// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCJR7A6J9K54GLkxtzyf2WZorYQcNYUlTs",
  authDomain: "poll-cfe5d.firebaseapp.com",
  databaseURL: "https://poll-cfe5d.firebaseio.com",
  projectId: "poll-cfe5d",
  storageBucket: "poll-cfe5d.appspot.com",
  messagingSenderId: "677554760787",
  appId: "1:677554760787:web:408caa4816179b90792aa6",
  measurementId: "G-XZVGJ380DJ"
}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
